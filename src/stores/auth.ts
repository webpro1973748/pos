import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
const currentUser = ref<User>({
    id: 1,
    email: 'pa123@gmail.com',
    password: 'Pass@123',
    fullName: 'Palee Mee',
    gender: 'male',
    roles: ['user']
})

  return { currentUser }
})
