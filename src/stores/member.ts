import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
  const member = ref<Member[]>([
    { id: 1, name: 'mb1', tel: '0123456789' },
    { id: 2, name: 'mb2', tel: '0123456788' }
  ])
  const currentMember = ref<Member | null>()
  const searchMember = (tel: string) => {
    const index = member.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = member.value[index]
  }
  function clear() {
    currentMember.value = null
  }
  return { member, currentMember, searchMember, clear }
})
